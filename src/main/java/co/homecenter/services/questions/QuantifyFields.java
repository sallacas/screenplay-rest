package co.homecenter.services.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static org.hamcrest.Matchers.*;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.*;

public class QuantifyFields implements Question<Boolean> {

    private final int quantity;
    public QuantifyFields(int quantity){
        this.quantity = quantity;
    }
    public static QuantifyFields are(int quantity) {
        return new QuantifyFields(quantity);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        actor.should(
                seeThatResponse("Validation quantity fields service response",
                        response -> response
                                .assertThat()
                                .body("size()", is(quantity))
                        )
        );
        return true;
    }
}
