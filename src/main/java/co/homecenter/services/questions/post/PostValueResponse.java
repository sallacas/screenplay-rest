package co.homecenter.services.questions.post;

import co.homecenter.services.models.dojo.TestData;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import static org.hamcrest.Matchers.*;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.*;
import static co.homecenter.services.utils.constantes.Constantes.VALIDATION_FIELDS_AND_VALUES;

public class PostValueResponse implements Question<Boolean> {
    public static PostValueResponse are() {
        return new PostValueResponse();
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        actor.should(
                seeThatResponse(String.format(VALIDATION_FIELDS_AND_VALUES,"post"),
                        response -> response.assertThat()
                                .body("name",equalTo(TestData.getData().get("name")))
                                .body("job",equalTo(TestData.getData().get("job")))
                )
        );
        return true;
    }
}
