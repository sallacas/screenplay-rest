package co.homecenter.services.questions.post;

import static co.homecenter.services.utils.constantes.Constantes.VALIDATION_FIELDS;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import static org.hamcrest.Matchers.*;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.*;
public class PostFields implements Question<Boolean> {
    public static PostFields areExpected() {
        return new PostFields();
    }
    @Override
    public Boolean answeredBy(Actor actor) {
        actor.should(
            seeThatResponse(String.format(VALIDATION_FIELDS, "post"),
                    response -> response.assertThat()
                            .body("$",hasKey("name"))
                            .and().body("$",hasKey("job"))
                            .and().body("$",hasKey("id"))
                            .and().body("$",hasKey("createdAt"))
                            .log().body()
            )
        );
        return true;
    }
}
