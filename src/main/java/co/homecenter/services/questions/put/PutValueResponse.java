package co.homecenter.services.questions.put;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.Matchers.equalTo;
import static co.homecenter.services.utils.constantes.Constantes.VALIDATION_FIELDS_AND_VALUES;
public class PutValueResponse implements Question<Boolean> {
    public static PutValueResponse are() {
        return new PutValueResponse();
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        actor.should(
                seeThatResponse(String.format(VALIDATION_FIELDS_AND_VALUES, "put"),
                        response -> response
                                .assertThat()
                                .body("name", equalTo("morpheus"))
                                .and().body("job", equalTo("leader"))
                                //.and().body("updatedAt", containsString(validation(Constantes.TIME_ZONE[0])))
                ));
        return true;
    }
}
