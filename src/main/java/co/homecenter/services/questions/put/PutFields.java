package co.homecenter.services.questions.put;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.homecenter.services.utils.constantes.Constantes.VALIDATION_FIELDS;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.Matchers.hasKey;

public class PutFields implements Question<Boolean> {
    public static PutFields areExpected() {
        return new PutFields();
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        actor.should(
                seeThatResponse(String.format(VALIDATION_FIELDS, "put"),
                        response -> response.assertThat()
                                .body("$",hasKey("name"))
                                .and().body("$",hasKey("job"))
                                .and().body("$",hasKey("updatedAt"))
                                .log().body()
                )
        );
        return true;
    }
}
