package co.homecenter.services.questions;

import static io.restassured.module.jsv.JsonSchemaValidator.*;

import co.homecenter.services.utils.constantes.Constantes;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.*;

public class SchemaIs implements Question<Boolean> {

    private final String schema;

    public SchemaIs(String schema){
        this.schema = schema;
    }
    public static SchemaIs expected(String schema) {
        return new SchemaIs(schema);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        actor.should(
                seeThatResponse("Validation schama service response",
                        response -> response
                                .assertThat()
                                .body(matchesJsonSchemaInClasspath(Constantes.getPathSchema(schema)))
                        )
        );
        return true;
    }
}
