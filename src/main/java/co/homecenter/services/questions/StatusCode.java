package co.homecenter.services.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.rest.questions.ResponseConsequence;

public class StatusCode implements Question<Boolean> {

    private final int code;

    public StatusCode(int code){
        this.code = code;
    }
    public static StatusCode is(int code) {
        return new StatusCode(code);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        actor.should(
                ResponseConsequence.seeThatResponse("API response status code",
                        response -> response
                                .statusCode(code)
                                .log().body()
                        )
        );
        return true;
    }
}
