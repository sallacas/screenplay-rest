package co.homecenter.services.questions.get;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import static org.hamcrest.Matchers.*;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.*;
import static co.homecenter.services.utils.constantes.Constantes.VALIDATION_FIELDS_AND_VALUES;
public class GetValueResponse implements Question<Boolean> {
    @Override
    public Boolean answeredBy(Actor actor) {
        actor.should(
                seeThatResponse(String.format(VALIDATION_FIELDS_AND_VALUES,"get"),
                        response -> response.assertThat()
                        .body("data.id",equalTo(1))
                        .and().body("data.email",containsString("@reqres.in"))
                        )
        );
        return true;
    }

    public static GetValueResponse are() {
        return new GetValueResponse();
    }
}
