package co.homecenter.services.questions.get;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import static org.hamcrest.Matchers.*;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.*;
import static co.homecenter.services.utils.constantes.Constantes.VALIDATION_FIELDS;
public class GetFields implements Question<Boolean> {

    public GetFields(){
        //Do nothing for x, y
    }
    public static GetFields areExpected() {
        return new GetFields();
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        actor.should(
                seeThatResponse(String.format(VALIDATION_FIELDS,"get"),
                        response -> response.assertThat()
                                .body("data",hasKey("id"))
                                .and().body("data",hasKey("email"))
                                .and().body("data",hasKey("first_name"))
                                .and().body("data",hasKey("last_name"))
                )
        );
        return true;
    }
}
