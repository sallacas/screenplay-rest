package co.homecenter.services.questions.get;

import co.homecenter.services.utils.GetListFrom;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import org.junit.Assert;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.*;
import static co.homecenter.services.utils.constantes.Constantes.VALIDATION_FIELDS;

public class GetListFields implements Question<Boolean> {
    public static GetListFields areExpected() {
        return new GetListFields();
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        List<String> fieldsProvide = Arrays.asList("id", "email", "first_name", "last_name", "avatar");
        actor.should(
                seeThatResponse(String.format(VALIDATION_FIELDS, "get list"),
                        response -> response.assertThat()
                                .body("$", hasKey("page"))
                                .and().body("$", hasKey("per_page"))
                                .and().body("$", hasKey("total"))
                                .and().body("$", hasKey("total_pages"))
                                .and().body("$", hasKey("data"))
                                .and().body("support", hasKey("url"))
                                .and().body("support", hasKey("text"))
                ));
        Assert.assertArrayEquals(
                GetListFrom.json("data").toArray(),
                GetListFrom.dataCompare(fieldsProvide).toArray()
        );
        return true;
    }
}
