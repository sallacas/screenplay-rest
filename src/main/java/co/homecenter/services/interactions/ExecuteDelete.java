package co.homecenter.services.interactions;

import co.homecenter.services.exceptions.ErrorServicesExpection;
import co.homecenter.services.models.dojo.TestData;
import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Delete;
import org.apache.http.HttpStatus;

public class ExecuteDelete implements Interaction {

    private final String resource;

    public ExecuteDelete(String resource) {
        this.resource = resource;
    }

    public static ExecuteDelete service(String resource) {
        return Tasks.instrumented(ExecuteDelete.class,resource);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        SerenityRest.reset();
        actor.attemptsTo(
                Delete.from(resource).with(
                        request -> request
                                .contentType(ContentType.JSON)
                                .params(TestData.getData())
                                .relaxedHTTPSValidation()
                )
        );
        if (SerenityRest.lastResponse().statusCode() != HttpStatus.SC_NO_CONTENT){
            throw new ErrorServicesExpection("Error executing DELETE");
        }
    }
}
