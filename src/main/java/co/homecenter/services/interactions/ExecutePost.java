package co.homecenter.services.interactions;

import co.homecenter.services.exceptions.ErrorServicesExpection;
import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;
import org.apache.http.HttpStatus;

public class ExecutePost implements Interaction {

    private final String resource;
    private final String body;

    public ExecutePost(String resource, String body) {
        this.resource = resource;
        this.body = body;
    }

    public static ExecutePost service(String resource, String body) {
        return Tasks.instrumented(ExecutePost.class,resource,body);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        SerenityRest.reset();
        actor.attemptsTo(
                Post.to(resource)
                        .with(request -> request
                                .contentType(ContentType.JSON)
                                .body(body)
                                .relaxedHTTPSValidation()
                        )
        );
        if (SerenityRest.lastResponse().statusCode() != HttpStatus.SC_CREATED) {
            throw new ErrorServicesExpection("Error al crear");
        }
    }
}
