package co.homecenter.services.interactions;

import co.homecenter.services.models.dojo.TestData;
import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Get;

public class ExecuteGet implements Task {

    private final String resources;

    @Override
    public <T extends Actor> void performAs(T actor) {
        SerenityRest.reset();
        actor.attemptsTo(
                Get.resource(resources)
                        .with(request -> request
                                .contentType(ContentType.JSON)
                                .params(TestData.getData())
                                .relaxedHTTPSValidation())
        );
    }
    public ExecuteGet(String resources){
        this.resources = resources;
    }
    public static ExecuteGet service(String resource) {
        return Tasks.instrumented(ExecuteGet.class,resource);
    }

}
