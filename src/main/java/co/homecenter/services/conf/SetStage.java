package co.homecenter.services.conf;

import co.homecenter.services.utils.constantes.Constantes;
import io.cucumber.java.Before;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.util.EnvironmentVariables;

public class SetStage {
    private EnvironmentVariables ev;
    @Before
    public void setStage(){
        OnStage.setTheStage(new OnlineCast());
        if (System.getProperty("env") != null){
            Constantes.setUrl(ev.getProperty("environments."+System.getProperty("env")+".base.url"));
            System.out.println("Environment: "+System.getProperty("env"));
        }else{
            Constantes.setUrl(ev.getProperty("environments.dev.base.url"));
            System.out.println("Environment: dev");
        }
    }
}
