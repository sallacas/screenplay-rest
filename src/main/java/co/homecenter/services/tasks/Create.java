package co.homecenter.services.tasks;

import static co.homecenter.services.utils.constantes.Constantes.*;
import co.homecenter.services.utils.templates.MergeFrom;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import java.util.Map;

public class Create implements Task {

    private final String template;
    private final Map<String, String> data;

    public Create(String template, Map<String, String> data) {
        this.template = template;
        this.data = data;
    }
    public static Create body(String template, Map<String, Object> data){
        return Tasks.instrumented(Create.class, template, data);
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        String templatePath = String.format(PATH_TEMPLATE,template);
        String body = MergeFrom.template(templatePath).withFieldsFrom(data);
        actor.remember(BODY,body);
    }
}
