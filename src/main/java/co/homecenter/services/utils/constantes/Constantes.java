package co.homecenter.services.utils.constantes;

public class Constantes {

    public static final String BODY = "body";
    public static final String PATH_TEMPLATE = "templates/%s";
    public static final String VALIDATION_FIELDS = "Validation fields %s service api response exists";
    public static final String VALIDATION_FIELDS_AND_VALUES = "Validation fields and values %s services response";
    public static final String[] TIME_ZONE = "Antarctica/Troll-America/Bogota".split("-");

    private static String url;
    public static String getPathSchema(String schema) {
        String pathSchema = "schemas/REPLACE.json";
        return pathSchema.replace("REPLACE", schema);
    }

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String url) {
        Constantes.url = url;
    }

    public Constantes(){
        //Do nothing
    }
}
