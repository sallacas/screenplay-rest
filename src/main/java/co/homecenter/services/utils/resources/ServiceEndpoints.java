package co.homecenter.services.utils.resources;

import co.homecenter.services.utils.constantes.Endpoints;

public enum ServiceEndpoints {

    URI(Endpoints.URL_USERS);

    private final String url;

    ServiceEndpoints(String url){
        this.url = url;
    }
    public String getUrl(){
        return url;
    }
}
