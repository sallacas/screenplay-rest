package co.homecenter.services.utils;

import net.serenitybdd.rest.SerenityRest;

import java.util.*;

public class GetListFrom {
    /*
    * Nos trae las keys en formato List<String> de la propiedad requerida en la ultima petición obtenida
    * */
    public static List<String> json(String primaryProperty){
        Set<Map.Entry<String, String>> dataAux = ((HashMap<String, String>)
                SerenityRest.lastResponse().jsonPath().getList(primaryProperty).get(0))
                .entrySet();
        List<String> keysData = new ArrayList<>();
        for (Map.Entry<String, String> entry : dataAux){
            keysData.add(entry.getKey());
        }
        Collections.sort(keysData);
        return keysData;
    }
    /*
    * Ordena el List<String> que le ingresemos
    * */
    public static List<String> dataCompare(List<String> primaryProperty){
        Collections.sort(primaryProperty);
        return primaryProperty;
    }
    private GetListFrom(){
        //Do nothing for x, y
    }
}
