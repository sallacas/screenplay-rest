package co.homecenter.services.exceptions;

public class ErrorServicesExpection extends RuntimeException{
    public ErrorServicesExpection(String message){
        super(message);
    }
}
