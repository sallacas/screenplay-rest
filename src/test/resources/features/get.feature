Feature: API Testing for service reqres.in

  @Get
  Scenario: Consume GET with response sucess
    Given i load customer information
    | id |
    | 1  |
    When i call get user API
    Then I should see the status code 200
    And i validate quantity key is 2
    And I validate schema response "GetJsonSchema"
    And I validate fields get response api
    And I validate get response contain data expected
