Feature: API Testing for service reqres.in

  @Post
  Scenario Outline: Consume POST with response success
    Given i load customer information
      | name | job |
      | <name>  | <job> |
    When i call post user API
    Then I should see the status code 201
    And i validate quantity key is 4
    And I validate schema response "PostJsonSchema"
    And I validate fields post response api
    And I validate post response contain data expected

    Examples:
      | name | job    |
   ##@externaldata@src/test/resources/datadriven/data.xlsx@Hoja1
   |John   |QA|
