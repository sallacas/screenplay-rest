Feature: API Testing for service reqres.in

  @Put
  Scenario: Consume PUT with response success
    Given i load customer information
      | name     | job    |
      | morpheus | leader |
    When i call Put user API with customer id 2
    Then I should see the status code 200
    And i validate quantity key is 3
    And I validate schema response "PutJsonSchema"
    And I validate fields put response api
    And I validate put response contain data expected
