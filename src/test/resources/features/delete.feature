Feature: API Testing for service reqres.in

  @Delete
  Scenario: Consume DELETE with response success
    Given i load customer information
      | id |
      | 1  |
    When i call delete user api
    Then I should see the status code 204

