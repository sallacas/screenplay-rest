Feature: API Testing for service reqres.in

  @Get
  Scenario: Consume GET ALL with response sucess
    Given i load customer information
      | page |
      | 1    |
    When i call get user API
    Then I should see the status code 200
    And i validate quantity key is 6
    And I validate schema response "GetListJsonSchema"
    And I validate fields get list response api
    And I validate get list response contain data expected
      | page | per_page | total | total_pages | support_url                        | support_text                                                             | id | email                  | first_name | last_name | avatar                                  |
      | 1    | 6        | 12    | 2           | https://reqres.in/#support-heading | To keep ReqRes free, contributions towards server costs are appreciated! | 1  | george.bluth@reqres.in | George     | Bluth     | https://reqres.in/img/faces/1-image.jpg |
