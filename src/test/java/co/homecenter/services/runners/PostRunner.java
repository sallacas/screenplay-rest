package co.homecenter.services.runners;

import co.homecenter.services.utils.exceldata.BeforeSuite;
import co.homecenter.services.utils.exceldata.DataToFeature;
import io.cucumber.junit.CucumberOptions;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.runner.RunWith;

import java.io.IOException;

/**
 *
 */
@RunWith(CustomRunner.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = "src/test/resources/features/post.feature",
        glue = {
                "co.homecenter.services.conf",
                "co.homecenter.services.stepdefinitions"
        }
)
public class PostRunner {
    @BeforeSuite
    public static void test() throws IOException, InvalidFormatException {
        DataToFeature.overrideFeatureFiles("src/test/resources/features/");
    }
}
