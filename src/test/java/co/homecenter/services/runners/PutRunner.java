package co.homecenter.services.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = "src/test/resources/features/put.feature",
        glue = {
                "co.homecenter.services.conf",
                "co.homecenter.services.stepdefinitions"
        }
)
public class PutRunner {
}
