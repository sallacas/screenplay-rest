package co.homecenter.services.stepdefinitions;

import co.homecenter.services.models.dojo.TestData;
import co.homecenter.services.questions.put.PutFields;
import co.homecenter.services.questions.put.PutValueResponse;
import co.homecenter.services.tasks.ConsumePut;
import co.homecenter.services.tasks.Create;
import co.homecenter.services.utils.resources.ServiceEndpoints;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import static net.serenitybdd.screenplay.GivenWhenThen.*;

public class PutStepDefinitions {

    @When("i call Put user API with customer id {int}")
    public void iCallPutUserAPIWithCustomerId(int id) {
        OnStage.theActorInTheSpotlight().attemptsTo(
                Create.body("singleuser.json", TestData.getData())
        );
        OnStage.theActorInTheSpotlight().attemptsTo(
                ConsumePut.service(ServiceEndpoints.URI.getUrl(),id)
        );
    }

    @And("I validate fields put response api")
    public void iValidateFieldsPutResponseApi() {
        OnStage.theActorInTheSpotlight().should(
                seeThat(PutFields.areExpected())
        );
    }

    @And("I validate put response contain data expected")
    public void iValidatePutResponseContainDataExpected() {
        OnStage.theActorInTheSpotlight().should(
                seeThat(PutValueResponse.are())
        );
    }
}
