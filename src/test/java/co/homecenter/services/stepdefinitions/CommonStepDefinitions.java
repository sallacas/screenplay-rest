package co.homecenter.services.stepdefinitions;

import co.homecenter.services.questions.QuantifyFields;
import co.homecenter.services.questions.SchemaIs;
import co.homecenter.services.questions.StatusCode;
import co.homecenter.services.tasks.Load;
import co.homecenter.services.utils.constantes.Constantes;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

import java.util.List;
import java.util.Map;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;

public class CommonStepDefinitions {
    @Given("i load customer information")
    public void iLoadCustomerInformation(List<Map<String, String>> data) {
        OnStage.theActorCalled("Bot").whoCan(CallAnApi.at(Constantes.getUrl()));
        OnStage.theActorInTheSpotlight().wasAbleTo(
                Load.testData(data.get(0))
        );
    }
    @Then("I should see the status code {int}")
    public void iShouldSeeTheStatusCode(int code) {
        OnStage.theActorInTheSpotlight()
                .should(seeThat(StatusCode.is(code))
                );
    }
    @Then("i validate quantity key is {int}")
    public void iValidateQuantityKeyIs(Integer quantity) {
        OnStage.theActorInTheSpotlight()
                .should(seeThat(QuantifyFields.are(quantity))
                );
    }
    @Then("I validate schema response {string}")
    public void iValidateSchemaResponse(String schema) {
        OnStage.theActorInTheSpotlight().should(
                seeThat(SchemaIs.expected(schema))
        );
    }
}
