package co.homecenter.services.stepdefinitions;

import co.homecenter.services.models.dojo.TestData;
import co.homecenter.services.questions.post.PostFields;
import co.homecenter.services.questions.post.PostValueResponse;
import co.homecenter.services.tasks.ConsumePost;
import co.homecenter.services.tasks.Create;
import co.homecenter.services.utils.resources.ServiceEndpoints;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import static net.serenitybdd.screenplay.GivenWhenThen.*;

public class PostStepDefinitions {
    @When("i call post user API")
    public void iCallPostUserAPI() {
        OnStage.theActorInTheSpotlight().attemptsTo(
                Create.body("singleuser.json", TestData.getData())
        );
        OnStage.theActorInTheSpotlight().attemptsTo(
                ConsumePost.service(ServiceEndpoints.URI.getUrl())
        );
    }

    @And("I validate fields post response api")
    public void iValidateFieldsPostResponseApi() {
        OnStage.theActorInTheSpotlight().should(
            seeThat(PostFields.areExpected())
        );
    }

    @And("I validate post response contain data expected")
    public void iValidatePostResponseContainDataExpected() {
        OnStage.theActorInTheSpotlight().should(
                seeThat(PostValueResponse.are())
        );
    }
}
