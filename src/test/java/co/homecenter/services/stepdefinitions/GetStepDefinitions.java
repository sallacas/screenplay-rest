package co.homecenter.services.stepdefinitions;


import co.homecenter.services.questions.get.GetListFields;
import co.homecenter.services.questions.get.GetFields;
import co.homecenter.services.questions.get.GetListValueResponse;
import co.homecenter.services.questions.get.GetValueResponse;
import co.homecenter.services.tasks.ConsumeGet;
import co.homecenter.services.tasks.Load;
import co.homecenter.services.utils.resources.ServiceEndpoints;
import io.cucumber.java.en.*;
import static net.serenitybdd.screenplay.GivenWhenThen.*;
import net.serenitybdd.screenplay.actors.OnStage;

import java.util.List;
import java.util.Map;

public class GetStepDefinitions {


    @When("i call get user API")
    public void iCallGetUserAPI() {
        OnStage.theActorInTheSpotlight().attemptsTo(
                ConsumeGet.service(
                        ServiceEndpoints.URI.getUrl()
                )
        );
    }
    @Then("I validate fields get response api")
    public void iValidateFieldsGetResponseApi() {
        OnStage.theActorInTheSpotlight().should(
                seeThat(GetFields.areExpected())
        );
    }
    @Then("I validate get response contain data expected")
    public void iValidateGetResponseContainDataExpected() {
        OnStage.theActorInTheSpotlight().should(
                seeThat(GetValueResponse.are())
        );
    }
    @And("I validate fields get list response api")
    public void iValidateFieldsGetListResponseApi() {
        OnStage.theActorInTheSpotlight().should(
                seeThat(GetListFields.areExpected())
        );
    }

    @And("I validate get list response contain data expected")
    public void iValidateGetListResponseContainDataExpected(List<Map<String, String>> data) {
        OnStage.theActorInTheSpotlight().wasAbleTo(
                Load.testData(data.get(0))
        );
        OnStage.theActorInTheSpotlight()
                .should(seeThat(GetListValueResponse.are()));
    }
}
