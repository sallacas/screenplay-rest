package co.homecenter.services.stepdefinitions;

import co.homecenter.services.tasks.ConsumeDelete;
import co.homecenter.services.utils.resources.ServiceEndpoints;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;

public class DeleteStepDefinitions {
    @When("i call delete user api")
    public void iCallDeleteUserApi() {
        OnStage.theActorInTheSpotlight().attemptsTo(
                ConsumeDelete.service(
                        ServiceEndpoints.URI.getUrl()
                )
        );
    }
}
